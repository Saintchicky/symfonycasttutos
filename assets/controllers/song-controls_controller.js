import { Controller } from '@hotwired/stimulus';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
import axios from 'axios';

export default class extends Controller {
    // déclaration de la valeur qu'on va récupérer
    static values = {
        infoUrl: String
    }
    // méthode qu'on va appeler ds le twig
    // cela gère le click event
    play(event) {
        event.preventDefault();
        //mettre infoUrl et Value à la fin
        axios.get(this.infoUrlValue)
            .then((response) => {
                // Class javascript Audio pour jouer le son
                const audio = new Audio(response.data.url);
                audio.play();
            });
    }
}